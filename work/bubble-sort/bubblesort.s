.globl pole

.text
.globl _start
.ent _start

_start:

addi $s0, $0, 14        // zbyvajici pocet pruchodu = delka pole -1
addi $s1, $0, 56       // konec pole = (delka pole -1) * 4
// $s2 aktualni pozice v poli
// $s3 prvni prvek pole
// $s4 druhy prvek pole

hlavni_cyklus:
     beq $s0, $0, end_loop  // probehly vsechny pruchody
     
     addi $s2, $0, 0    // aktualni pozice v poli
     
     vnitrni_cyklus:
          
          lw $s3, pole($s2)
          lw $s4, pole+4($s2)
          
          slt $at, $s3, $s4
          bne $at, $0, neprehazovat
          
          sw $s4, pole($s2)   // prehozeni prvku pole
          sw $s3, pole+4($s2)
          
     neprehazovat:
          addi $s2, $s2, 4    // posun na dalsi pozici
          bne $s2, $s1, vnitrni_cyklus
          addi $s1, $s1, -4

     addi $s0, $s0, -1
     j hlavni_cyklus
     
end_loop:
     cache 9, 0($0)    // vyprazdneni cache
     break             // zastaveni simulatoru
     j end_loop
     nop
.end .start

.org 0x2001

.data
// Pro spusteni je nutne nastavit jadro s delay-slotem

pole:
.word   5, 3, 4, 1, 15, 8, 9, 2, 10, 6, 11, 1, 6, 9, 12

#pragma qtmips focus memory apole
